some test job:
  image: python:3.10
  script:
    - python --version
    - pip --version
    - pip install pytest
    - pytest --version
    - echo "doing complex stuff"


build image:
  image: docker
  services:
    - docker:dind
  script:
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER $CI_REGISTRY --password-stdin
    - docker build -t $CI_REGISTRY_IMAGE .
    - docker push $CI_REGISTRY_IMAGE
